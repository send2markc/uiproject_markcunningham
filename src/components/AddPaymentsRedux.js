import React, { useState } from "react";
import PropTypes from "prop-types";
import paymentAdd from "../redux/actions/PaymentAddActions";
import {useDispatch} from "react-redux";

import connect from "react-redux/lib/connect/connect";
import paymentList from "../redux/actions/PaymentListActions";

    function AddPaymentsRedux(props) {
        const dispatch = useDispatch();
        const initialFormState = {
            id: 0,
            paymentDate: new Date(),
            type: "",
            amount:0,
            submitted: false
             };
        const [payment, setPayment] = useState(initialFormState);
        const handleInputChange = (event) => {
            const { name, value } = event.target;

            setPayment({ ...payment, [name]: value });
        };


        return (<form
                onSubmit={(event) => {
                event.preventDefault();
                    dispatch(paymentAdd(payment));
                    // const onNewEmployeeSubmitHandler = (employee) => {
                    //     dispatch(employeeAdd(employee));
                    // };

                    // props.onNewEmployeeSubmitHandler(employee);
             }}>
                <h1>Add Payments</h1>
                {/*<div className="form-group">*/}
                {/*    <label htmlFor="exampleFirstname">Payment Date</label>*/}
                {/*    <input type="text" className="form-control" id="exampleFirstname" aria-describedby="fnameHelp"*/}
                {/*           placeholder="Enter name" value={this.state.paymentDate} onChange={this.onChangePaymentDate}/>*/}
                {/*    <small id="fnameHelp" className="form-text text-muted"></small>*/}
                {/*</div>*/}
                {/*<div className="form-group">*/}
                {/*    <label htmlFor="exampleLastname">Type</label>*/}
                {/*    <input type="text" className="form-control" id="exampleLastname" aria-describedby="lnameHelp"*/}
                {/*           placeholder="Enter name" value={this.state.type} onChange={this.onChangeType}/>*/}
                {/*    <small id="lnameHelp" className="form-text text-muted"></small>*/}
                {/*</div>*/}
                {/*<div className="form-group">*/}
                {/*    <label htmlFor="exampleSalary">Amount</label>*/}
                {/*    <input type="number" className="form-control" id="exampleSalary" aria-describedby="emailHelp"*/}
                {/*           placeholder="Enter salary" value={this.state.amount} onChange={this.onChangeAmount}/>*/}
                {/*    <small id="emailSalary" className="form-text text-muted"></small>*/}
                {/*</div>*/}
                {/*<div className="form-group">*/}
                {/*    <label htmlFor="exampleid">Id</label>*/}
                {/*    <input type="number" className="form-control" id="exampleid" aria-describedby="emailid"*/}
                {/*           placeholder="Enter id" value={this.state.id} onChange={this.onChangeId}/>*/}
                {/*</div>*/}
                {/*<div className="form-group">*/}

                {/*    <input type="submit" className="form-control" value="Save" />*/}
                {/*</div>*/}

            </form>
        );
    }

// AddEmployeesRedux.propTypes = {
//     onSubmitHandler: PropTypes.func.isRequired,
// };


// const mapDispatchToProps = (dispatch) => {
//     return {
//         dispatch: () => dispatch(paymentList())
//     };
// };
//
//
// const mapStateToProps = state => ({
//     error:state.error,
//     payments: state.payments,
//     pending: state.pending
// })

//export default connect(mapStateToProps,mapDispatchToProps)(AddPaymentsRedux);
export default AddPaymentsRedux;
