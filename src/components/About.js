import React, {Component} from 'react';

export default class About extends Component {
    constructor( props ) {
        super( props );
        this.state = { name: "Allstate", year: "2020", apistatus:"Unknown" };
    }
    render() {
        return(
            <div>
                {/*<h1>Name is {this.state.name}</h1>*/}
                {/*<h2>Year is {this.state.year}</h2>*/}
                <h3>Status is {this.state.apistatus}</h3>
            </div>)
    }


    async componentDidMount() {
        var url = "http://localhost:8080/api/status"
        let textData
        try {
            console.log('start');

            let response = await fetch(url);
            textData = await response.text();
            console.log('The data is: ' + textData)
        }
        catch(e) {
            console.log('The err is: ' + e.toString())

        }
        this.setState({apistatus: textData})
        //return textData;

    }
}