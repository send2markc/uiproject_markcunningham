import React, {Component} from 'react'
import paymentList from "../redux/actions/PaymentListActions"
import connect from "react-redux/lib/connect/connect";
import $ from "jquery"
import "../App.css"
import Card from "../shared/card"

 class ListPaymentsRedux extends Component {

    constructor(props) {
        super(props);
        console.log("Before" + this.props)
        this.props.dispatch(paymentList())
        console.log("After" + this.props)
    }

componentDidMount() {

    // $('li.submenu').on('click','a[href="#Details"]',function(e){
    //     e.preventDefault();
    //     console.log("on click called");
    //
    //     $("ul.ul_submenu").toggle();
    //     console.log("toggle");
    // })
}

     render() {
         // $('li.submenu').one('click','a[href="#Details"]',function(e){
         //     e.preventDefault();
         //     console.log("on click called");
         //
         //     $("ul.ul_submenu").toggle();
         //     console.log("toggle");
         // })

        // will get initial state from props
        const {payments,pending,error} = this.props;
        console.log("Payment:- " + payments)
        return (

            <div className="col-md-9">
                <h4>Payments List</h4>

                <ul className="list-group">
                    {payments &&
                    payments.map((payment, index) => (
                        // <li
                        //     className={
                        //         "list-group-item " +
                        //         (index === 1 ? "" : "")
                        //     }
                        //     // onClick={() => this.setActiveTutorial(tutorial, index)}
                        //     key={index}
                        // >
                        //     <b>Payment Date:-</b>  {new Date(payment.paymentdate).toLocaleDateString('en-US')}  <b>Payment Type:-</b>  {payment.type}  <b>Payment Amount:-</b>   {payment.amount}
                        // </li>
                        <Card>
                            <div className="col-md-9">
                                <b>Payment Date:-</b>  {new Date(payment.paymentdate).toLocaleDateString('en-US')} <div className="myDIV">Details</div>

                                <div className="hide"><b>Payment Type:-</b>  {payment.type}  <b>Payment Amount:-  {payment.amount}</b></div>
                            </div>

                            {/*<b>Payment Date:-</b>  {new Date(payment.paymentdate).toLocaleDateString('en-US')}  <b>Payment Type:-</b>  {payment.type}  <b>Payment Amount:-</b>*/}
                        </Card>
                            // <li
                        //     className={
                        //         "list-group-item " +
                        //         (index === 1 ? "" : "")
                        //     }
                        //     // onClick={() => this.setActiveTutorial(tutorial, index)}
                        //     key={index}
                        // >
                        //     <b>Payment Date:-</b>{new Date(payment.paymentdate).toLocaleDateString('en-US')}
                        //     <ul>
                        //         <li className="submenu"><a href="#Details">Details</a>
                        //             <ul className="ul_submenu">
                        //                 <li>
                        //                     <div id="paymentDetails"><b>Payment Type:-</b>  {payment.type}  <b>Payment Amount:-</b>   {payment.amount}</div>
                        //                 </li>
                        //             </ul>
                        //         </li>
                        //     </ul>
                        // </li>





                    ))}
                </ul>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};


const mapStateToProps = state => ({
    error:state.error,
    payments: state.payments,
    pending: state.pending
})

export default connect(mapStateToProps,mapDispatchToProps)(ListPaymentsRedux);
//export default ListEmployees;