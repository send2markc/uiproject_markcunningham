import React, {Component} from 'react'
import PaymentRestService from "../services/PaymentRestService";
import Redirect from "react-router-dom/es/Redirect";
import moment from "moment";

export default class AddPayments extends Component {
    constructor(props) {
        super(props);
        this.onChangePaymentDate = this.onChangePaymentDate.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeId = this.onChangeId.bind(this);
        this.savePayment = this.savePayment.bind(this);

        this.state = {
            id: 0,
            paymentDate: new Date().toLocaleDateString('en-US'),
            type: "",
            amount:0,
            submitted: false
        };
    }

    onChangePaymentDate(e) {
        this.setState({
            paymentDate: e.target.value

        });
    }


    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }

    onChangeId(e) {
        this.setState({
            id: e.target.value
        });
    }


    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }

    savePayment() {
        var payDate = moment(new Date(this.state.paymentDate)).format('YYYY-MM-DD[T00:00:00.000Z]');

        var data = {
            id: this.state.id,
            paymentdate: moment(new Date(this.state.paymentDate)).format('YYYY-MM-DD[T00:00:00.000Z]'),
            type: this.state.type,
            amount: this.state.amount        }

        console.log( "save payment data object:" + data.paymentDate)

        // {"id":1,"fname":"deirdre","lname":"geary","salary":0,"email":"d@i.ie"}
        PaymentRestService.create(data)
            .then(response => {
                this.setState({
                    // id: response.data.id,
                    // fname: response.data.fname,
                    // lname: response.data.lname,
                    // email: response.data.email,
                    // salary: response.data.salary,
                    submitted: true

                });

            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {

        if ( this.state.submitted)
        {
            return <Redirect to="home" />
        }
        return (
            <form>
                <h1>Add Payments</h1>
                <div className="form-group">
                    <label htmlFor="exampleFirstname">Payment Date</label>
                    <input type="text" className="form-control" id="exampleFirstname" aria-describedby="fnameHelp"
                           placeholder="Enter payment date" value={this.state.paymentDate} onChange={this.onChangePaymentDate}/>
                    <small id="fnameHelp" className="form-text text-muted"></small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleLastname">Type</label>
                    <input type="text" className="form-control" id="exampleLastname" aria-describedby="lnameHelp"
                           placeholder="Enter payment type" value={this.state.type} onChange={this.onChangeType}/>
                    <small id="lnameHelp" className="form-text text-muted"></small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleSalary">Amount</label>
                    <input type="number" className="form-control" id="exampleSalary" aria-describedby="emailHelp"
                           placeholder="Enter salary" value={this.state.amount} onChange={this.onChangeAmount}/>
                    <small id="emailSalary" className="form-text text-muted"></small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleid">Id</label>
                    <input type="number" className="form-control" id="exampleid" aria-describedby="emailid"
                           placeholder="Enter id" value={this.state.id} onChange={this.onChangeId}/>
                </div>
                <div className="form-group">

                    <input type="button" className="form-control" value="Save" onClick={this.savePayment}/>
                </div>

            </form>
        )
    }

};

//export default AddEmployees;