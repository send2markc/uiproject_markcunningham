import React, {Component} from 'react'
import PaymentRestService from "../services/PaymentRestService";

export default class ListPayments extends Component {

    constructor(props) {
        super(props);
        this.state = {
            payments: [],
            currentIndex: -1}
    }


    // invoke axios method to get api
    retrievePayments() {
        PaymentRestService.getAll()
            .then(response => {
                this.setState({
                    payments: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    // called after render
    componentDidMount() {
        this.retrievePayments();
    }




    render() {
        const {payments, currentIndex} = this.state;
        return (
            <div className="col-md-9">
                <h4>Payments List</h4>

                <ul className="list-group">
                    {payments &&
                    payments.map((payment, index) => (
                        <li
                            className={
                                "list-group-item " +
                                (index === currentIndex ? "active" : "")
                            }
                            // onClick={() => this.setActiveTutorial(tutorial, index)}
                            key={index}
                        >
                            <b>Payment Date:-</b>  {new Date(payment.paymentdate).toLocaleDateString('en-US')}  <b>Payment Type:-</b>  {payment.type}  <b>Payment Amount:-</b>   {payment.amount}
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
};

//export default ListEmployees;