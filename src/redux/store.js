import {applyMiddleware, combineReducers, createStore} from "redux";
import  PaymentListReducer from "./reducers/PaymentListReducer"
import  PaymentAddReducer from "./reducers/PaymentAddReducer"
import CombineReducer from "./reducers/CombineReducer";
import thunk from "redux-thunk";
const initialState = {};
export const middlewares = [thunk];
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ //|| compose;

const rootReducer = combineReducers({PaymentListReducer , PaymentAddReducer})
export const store = createStore(
    PaymentListReducer, //typically combine all reducers
    initialState,
    //  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    composeEnhancer(applyMiddleware(...middlewares))
);
