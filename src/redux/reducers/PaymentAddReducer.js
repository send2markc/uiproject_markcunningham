import {PAYMENT_ADD_REQUEST , PAYMENT_ADD_SUCCESS , PAYMENT_LIST_FAIL}
    from "../constants/PaymentConstants"




const paymentsInitialState = {
    payment: {},
    error: "",
    submitted: false,
};

function paymentAddReducer(
    state = paymentsInitialState,
    action
) {
    switch (action.type) {
        case PAYMENT_ADD_REQUEST:
            return { ...state, payment: action.payload };
        case PAYMENT_ADD_SUCCESS:
            return { ...state, submitted: true, payment: action.payload };
        case PAYMENT_LIST_FAIL:
            return { ...state, submitted: true, error: action.payload };
        default:
            return state;
    }
}
export default paymentAddReducer