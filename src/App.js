import React from 'react';
import logo from './logo.svg';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route, Link } from "react-router-dom";
import AddPayments from "./components/AddPayments"
//import AddPaymentsRedux from "./components/AddPaymentsRedux"
import ViewPayments from "./components/ViewPayments"
import ListPayments from "./components/ListPayments"
import ListPaymentsRedux from "./components/ListPaymentsRedux"
//import Welcome from "./components/Welcome"
import About from "./components/About"
import Provider from "react-redux/lib/components/Provider";

function App() {
  return (
      <div id="container">
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <a href="/home" className="navbar-brand">
            Home
          </a>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/payments"} className="nav-link">
                Payments
              </Link>
            </li>
            {/*<li className="nav-item">*/}
            {/*  <Link to={"/add"} className="nav-link">*/}
            {/*    Add Payments Redux*/}
            {/*  </Link>*/}
            {/*</li>*/}

            <li className="nav-item">
              <Link to={"/add"} className="nav-link">
                Add Payments
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/ListPaymentsRedux"} className="nav-link">
                List Payments Redux
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/about"} className="nav-link">
                About
              </Link>
            </li>
          </div>
        </nav>
        <div className="container mt-3">
          <Switch>

            <Route exact path={["/", "/payments"]} component={ListPayments} />
            <Route exact path="/add" component={AddPayments} />
            {/*<Route exact path="/add" component={AddPaymentsRedux} />*/}
            <Route exact path="/about" component={About} />
            <Route exact path="/ListPaymentsRedux" component={ListPaymentsRedux} />
            <Route path="/payments/:id" component={ViewPayments} />
          </Switch>
        </div>

      </div>
  );
}

export default App;
